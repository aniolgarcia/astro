import sys
import csv
import math
from PIL import Image, ImageDraw


def altaz(ra, dec, lat):
    sin_alt = math.sin(dec)*math.sin(lat) + math.cos(dec)*math.cos(lat)*math.cos(ra)
    alt = math.asin(sin_alt)
    y = -math.cos(dec)*math.cos(lat)*math.sin(ra)
    x = math.sin(dec) -math.sin(lat)*sin_alt
    az = math.atan2(y,x)

    return (alt, az)

def stereo(alt, az):
    zalt = math.pi/2
    x = math.cos(az) * math.tan((zalt - alt)/2)
    y = math.sin(az) * math.tan((zalt - alt)/2)
    return (x,y)


resolution = 1200
margin = 50
bound_rad = resolution/2 - margin

im = Image.new('RGB', (resolution, resolution), color = 'white')

draw = ImageDraw.Draw(im)

rads = math.pi/180
phi = 40.0
lst = 9.0*15*rads

draw.ellipse((margin, margin, resolution-margin, resolution-margin),fill = 'white', outline='red')
draw.point((resolution/2, resolution/2), 'red')

x,y = stereo(0,0)
print(x,y)
draw.text((x*bound_rad + resolution/2 ,-y*bound_rad + resolution/2),"N", fill='black')
x,y = stereo(0,90*rads)
print(x,y)
draw.text((x*bound_rad + resolution/2 ,-y*bound_rad + resolution/2),"E", fill='black')
x,y = stereo(0,180*rads)
print(x,y)
draw.text((x*bound_rad + resolution/2 ,-y*bound_rad + resolution/2),"S", fill='black')
x,y = stereo(0,270*rads)
print(x,y)
draw.text((x*bound_rad + resolution/2 ,-y*bound_rad + resolution/2),"0", fill='black')

with open("myyale.csv", mode='r') as file:
    csvFile = csv.reader(file)
    for line in csvFile:
        ra = float(line[0])*rads*15
        dec = float(line[1])*rads
        mag = float(line[2])
        alt, az = altaz(9*15*rads - ra, dec, phi*rads)
        print(ra, dec, alt, az)
        if alt > 0:
            radius = 1*math.sqrt(5 - mag)
            x, y = stereo(alt, az)
            prop_x = bound_rad*x + resolution/2
            prop_y = -bound_rad*y + resolution/2
            draw.ellipse((prop_x - radius, prop_y - radius, prop_x + radius, prop_y + radius), fill='black', outline='black')


with open("conlines.csv", mode='r') as file:
    csvFile = csv.reader(file)
    cons = ""
    for line in csvFile:
        if line[0] != "":
            #print(line)
            ra1 = float(line[1])/1000 * rads * 15
            dec1 = float(line[2])/100 * rads
            ra2 = float(line[3])/1000 * rads * 15
            dec2 = float(line[4])/100 * rads
            alt1, az1 = altaz(lst - ra1, dec1, phi*rads)
            alt2, az2 = altaz(lst - ra2, dec2, phi*rads)

            if alt1 > 0 and alt2 > 0:
                x1, y1 = stereo(alt1, az1)
                x2, y2 = stereo(alt2, az2)
                draw.line((bound_rad*x1+resolution/2, -bound_rad*y1 + resolution/2, bound_rad*x2 + resolution/2,-bound_rad*y2 + resolution/2), fill='blue', width=2)






# write to stdout
im.save("im.png")



