size(16cm, 0);
import solids;
import graph3;
import three;

//currentprojection=orthographic(15,10,5);
currentprojection=orthographic(2, -10, 5);

real r=10; // r=sphere radius


//Defining pucture limits
limits((0,0,0),1.1*(r,r,r));

//Definition of red pen 
pen p=rgb(1,0,0);

pen g=rgb(0.5,2,0.5);


//Drawing the axis
draw(Label("$N=0^{\circ}$", 1), O--r*X, p, Arrow3);
draw(Label("$S=180^{\circ}$", 1), O--(-r*X), p, Arrow3);
draw(Label("$W=270^{\circ}$", 1), O--r*Y, p+dashed, Arrow3);
draw(Label("$E=90^{\circ}$", 1), O--(-r*Y), p+dashed, Arrow3);


path3 arcU=Arc(O,r,90,0,0,0,Y,10); //Arc of revolution
revolution sphereU=revolution(O,arcU,Z); //Revolute arc arround Z axis
draw(surface(sphereU), opacity(0.25)+palegrey); //Drawing semisphere with opacity

real width = 5;
real alt = 40; //Altitude of star
real az = 250; //Azimuth of star
real cnp_angle = 30;

triple star = r*dir(90 - alt, az); //Drawing the star ((0,0) being zenith, not (0,0) in alt-az system
dot("s", star, NW, linewidth(width)+mediumred);

//Drawing the celestial north pole
triple cnp = r*dir(cnp_angle,0);
dot("CNP", cnp, N, linewidth(width));

//Drawing the zenith point 
dot("zenith", r*dir(0, 0), N, linewidth(width));

//Drawing the projection of the arc that passes thru zenith and star
draw((0,0,0)-- r*dir(90, az), dashed);

//Drawing the arc that passes thru zenith and star
draw(Label("altitude", 1.8), arc(O,r*Z,r*dir(90,az)),royalblue+dashed);

//Drawing zenith-CNP-N arc
draw(arc(O, r*Z, r*X), dashed);

//Drawing celestial equator, using cnp as normal
path3 cel_eq =  arc(O,r*Y,-r*Y, normal=cnp, CCW);
draw("celestial equator", cel_eq);
//Drawing center-star segment
draw(O--star,Arrow3,PenMargin3);

//Altitude arc segment
draw("$a$",arc(O,2*0.15*r*dir(90,az), 0.15*star,CCW),E, arrow = Arrow3(TeXHead2), royalblue);

//Azimuth arc segment
path3 az_path = arc(O, 2*0.15*r, 90,0,90,az,CCW);
draw(az_path, arrow = Arrow3(TeXHead2), heavymagenta);
draw(Label("$A$"), az_path, N, heavymagenta);
draw(Label("azimuth"), az_path, S, heavymagenta);
//draw(Label("azimuth ($A$)", 1), , S, arrow = Arrow3(TeXHead2), heavymagenta); 

