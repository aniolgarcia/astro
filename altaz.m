function altaz(ra, dec, lat, lst)
  x = sin(pi/2 - dec)*sin(lst - ra);
  y = sin(pi/2 - lat)*cos(pi/2 - dec) - cos(pi/2 - lat)*sin(pi/2 - dec)*cos(lst - ra);
  atan2(y,x) - pi/2
  z = cos(pi/2 -dec)*cos(pi/2 - lat) + sin(pi/2 - dec)*sin(pi/2 - lat)*cos(lst - ra);
  zen = acos(z);
  pi/2 - zen 
  x = cos(pi/2 - dec) - cos(pi/2 - lat)*cos(zen);
  y = sin(pi/2 - lat)*sin(zen);
  acos(x/y)
endfunction
