DECLARE FUNCTION between! (a!, b!, x!)
DECLARE FUNCTION beyer$ (in$)
DECLARE SUB dTextRotate (x!, y!, text$, fontsize!, font$, angle!)
DECLARE SUB dTextRgreek (x!, y!, text$, fontsize!, angle!)
DECLARE SUB dCircleFill (centerX!, centerY!, radius!)
DECLARE SUB stereo (a!, z!, x!, y!)
DECLARE SUB altaz (h!, d!, a!, z!, glat!)
DECLARE FUNCTION atn2! (x!, y!)
DECLARE FUNCTION acos! (x!)
DECLARE FUNCTION asin! (x!)
DECLARE FUNCTION atan2 (y!, x!)
DECLARE SUB INIT (a$)
DECLARE SUB dLine (x1!, y1!, x2!, y2!)
DECLARE SUB dpoint (x!, y!)
DECLARE SUB dCircle (x!, y!, r!)
DECLARE SUB dArc (x!, y!, r!, ang1!, ang2!)
DECLARE SUB dRect (x1!, y1!, x2!, y2!)
DECLARE SUB dText (x!, y!, text$, fontsize!)
DECLARE SUB endsec ()
DECLARE SUB dClose ()
CLS
PRINT "PS Horizon Star Map"
PRINT "-------------------"
PRINT

'   constants
'
pi = 4 * ATN(1)
rads = pi / 180
degs = 180 / pi
scale = 250
offset = 300
nang = -90
x = 0: y = 0: a = 0: z = 0
'
'   get latitude and sidereal time for chart
'
INPUT "Latitude : ", phi
INPUT "Sidereal time in hours : ", LST
INPUT "PS file name : ", psfile$
IF ABS(phi) > 90 THEN phi = 52.5
phi = phi * rads
IF LST < 0 OR LST > 24 THEN LST = 9
LST = LST * 15 * rads
CALL INIT(psfile$)
'
'   draw the horizon circle
'
CALL dCircle(offset, offset, scale)
'
'   label the compass points. Remember that you look up
'   into a star map - hold map overhead and point North
'   to North and the order of NESW looks sensible.
'
CALL stereo(0, 0, x, y)
CALL dText(x * scale + offset, y * scale + offset, "N", 10)
CALL stereo(0, 90 * rads, x, y)
CALL dText(x * scale + offset, y * scale + offset, "E", 10)
CALL stereo(0, 180 * rads, x, y)
CALL dText(x * scale + offset, y * scale + offset, "S", 10)
CALL stereo(0, 270 * rads, x, y)
CALL dText(x * scale + offset, y * scale + offset, "W", 10)
'
'Now draw the stars...get Ra and Dec then convert to Alt Az
'then if star is above horizon, project and plot as circle
'of size equal to magnitude.
'
a$ = "YALE"
OPEN a$ + ".dat" FOR INPUT AS #2
DO WHILE NOT EOF(2)
INPUT #2, r, d, m
r = r * rads * 15
d = d * rads
CALL altaz(LST - r, d, a, z, phi)
IF a > 0 THEN
        rs = .75 * SQR(5 - m)
        CALL stereo(a, z, x, y)
        CALL dCircleFill(scale * x + offset, scale * y + offset, rs)
END IF
LOOP
CLOSE #2
'complete the stars file
PRINT "Stars done"
'
' Now for constellation lines. First attempt at clipping
' constellaton figures properly. I've left out cases IV
' and V as being so very rare (and a case IV would probably
' look a little strange on the chart anyway)
'
a1 = 0: a2 = 0: z1 = 0: z2 = 0: x1 = 0: y1 = 0: x2 = 0: y2 = 0
a$ = "CONLINES"
OPEN a$ + ".dat" FOR INPUT AS #2
DO WHILE NOT EOF(2)
        plotted = 0
        INPUT #2, cname$, r1, d1, r2, d2
        r1 = r1 / 1000 * rads * 15
        d1 = d1 / 100 * rads
        r2 = r2 / 1000 * rads * 15
        d2 = d2 / 100 * rads
        CALL altaz(LST - r1, d1, a1, z1, phi)
        CALL altaz(LST - r2, d2, a2, z2, phi)
        ' Case II
        ' if both ends of the line are above the horizon
        ' then plot line
        IF a1 > 0 AND a2 > 0 THEN
                CALL stereo(a1, z1, x1, y1)
                CALL stereo(a2, z2, x2, y2)
                CALL dLine(scale * x1 + offset, scale * y1 + offset, scale * x2 + offset, scale * y2 + offset)
                plotted = 1
        END IF
        ' case III
        ' if one end below, and one end above, then
        ' find point of intersection with horizon and plot from
        ' intersection point and end that is above horizon
        IF plotted <> 1 AND (a1 * a2) < 0 THEN
               ' sort out which end is which
                IF a1 > 0 THEN
                        atop = a1
                        ztop = z1
                        abot = a2
                        zbot = z2
                ELSE
                        atop = a2
                        ztop = z2
                        abot = a1
                        zbot = z1
                END IF
                '
                ' project onto plane as x,y  coords
                '
                CALL stereo(atop, ztop, x1, y1)
                CALL stereo(abot, zbot, x2, y2)
                '
                ' find gradient and intercept of line
                '
                m = (y1 - y2) / (x1 - x2)
                c = y1 - m * x1
                '
                ' find coefficients of the quadratic
                ' equation to find the intercept
                ' point
                '
                aq = (m * m + 1)
                bq = 2 * c * m
                cq = c * c - 1
                '
                ' check the discriminant and catch
                ' case V
                '
                dg = bq * bq - 4 * aq * cq
                ' get roots and sort which root to take
                xr1 = (-bq + SQR(dg)) / (2 * aq)
                xr2 = (-bq - SQR(dg)) / (2 * aq)
                yr1 = m * xr1 + c
                yr2 = m * xr2 + c
                p = between(x1, x2, xr1)
                q = between(y1, y2, yr1)
                IF p > 0 AND q > 0 THEN
                        x2 = xr1
                        y2 = yr1
                END IF
                p = between(x1, x2, xr2)
                q = between(y1, y2, yr2)
                IF p > 0 AND q > 0 THEN
                        x2 = xr2
                        y2 = yr2
                END IF
                ' draw the clipped line
                CALL dLine(scale * x1 + offset, scale * y1 + offset, scale * x2 + offset, scale * y2 + offset)
                plotted = 1
        END IF
        LOOP
CLOSE #2
PRINT "constellations done"
'
'   Now for constellation names. These plot a bit like stars, but
'   label text is rotated by Azimuth angle.
'
a$ = "CNAMES"
nang = -90
OPEN a$ + ".dat" FOR INPUT AS #2
DO WHILE NOT EOF(2)
        INPUT #2, r, d, cname$
        r = r / 1000 * rads * 15
        d = d / 100 * rads
        CALL altaz(LST - r, d, a, z, phi)
        IF a > 0 THEN
                textang = nang + z * degs
                CALL stereo(a, z, x, y)
                CALL dTextRotate(scale * x + offset, scale * y + offset, cname$, 6, "Helvetica", textang)
        END IF
        LOOP
CLOSE #2
PRINT "constellation name labels done"
'
'       Now do the Beyer letter labels
'       essentially identical to constellation labels
'       except for some offset to avoid the labels
'       overlapping the star circles
'
a$ = "beyer"
nang = -90
OPEN a$ + ".dat" FOR INPUT AS #2
xnudge = 2 / scale
ynudge = 2 / scale
DO WHILE NOT EOF(2)
        INPUT #2, letter$, r, d
        r = r * rads * 15
        d = d * rads
        CALL altaz(LST - r, d, a, z, phi)
        IF a > 0 THEN
                z = z
                textang = nang + z * degs
                CALL stereo(a, z, x, y)
                x = x + SGN(x) * xnudge
                y = y + SGN(y) * ynudge
                CALL dTextRotate(scale * x + offset + nudge, scale * y + offset + nudge, beyer(letter$), 6, "Symbol", textang)
        END IF
        LOOP
CLOSE #2
PRINT "Beyer stars done"

'
'   draw a small cross at the Zenith
'
CALL dLine(offset - 5, offset, offset + 5, offset)
CALL dLine(offset, offset - 5, offset, offset + 5)
'
'   label the chart with latitude and Sidereal time
'
IF phi > 0 THEN hname$ = "N" ELSE hname$ = "S"
CALL dText(offset - .95 * scale, 2 * offset - .2 * scale, "Latitude: " + STR$(phi * degs) + hname$, 12)
CALL dText(offset - .95 * scale, 2 * offset - .25 * scale, "Sidereal time: " + STR$(LST * degs / 15) + "h", 12)
CALL dClose
END
'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++

'Some maths and astronomical functions...

FUNCTION acos (x)
'returns arccos of angle in radians
s = SQR(1 - x * x)
acos = ATN(s / x)
END FUNCTION

SUB altaz (h, d, a, z, glat)
'finds the horizon coords given the ha and dec
sina = SIN(d) * SIN(glat) + COS(d) * COS(glat) * COS(h)
a = asin(sina)
y = -COS(d) * COS(glat) * SIN(h)
x = SIN(d) - SIN(glat) * sina
z = atan2(y, x)
END SUB

FUNCTION asin (x)
'returns the arcsin of x in radians
c = SQR(1 - x * x)
asin = ATN(x / c)
END FUNCTION

FUNCTION atan2 (y, x)
'returns the inverse tan atan(y/x) in the
'correct quadrant
a = ATN(y / x)
pi = 4 * ATN(1)
IF x < o THEN a = a + pi
IF y < 0 AND x > 0 THEN a = a + pi + pi
atan2 = a
END FUNCTION

FUNCTION atn2 (x, y)
'returns the arctangent in the correct quadrant
a = ATN(y / x)
pi = 4 * ATN(1)
IF x < o THEN a = a + pi
IF y < 0 AND x > 0 THEN a = a + pi + pi
atn2 = a
END FUNCTION

FUNCTION between (a, b, x)
'returns 1 if x is within the interval a,b
'returns 0 otherwise
a1 = a: b1 = b
IF a1 > b1 THEN
        c = b1
        b1 = a1
        a1 = c
END IF
IF a1 < x AND x < b1 THEN
        between = 1
ELSE
        between = 0
END IF
END FUNCTION

FUNCTION beyer$ (in$)
'very stupid function that looks up the beyer
'letter in roman (ie gamma) and substitutes
'the appropriate letter for symbol font
'you may have to change this for other greek
'fonts on different systems
beyer = ""
IF in$ = "Alpha" THEN beyer = "a"
IF in$ = "Beta" THEN beyer = "b"
IF in$ = "Chi" THEN beyer = "c"
IF in$ = "Delta" THEN beyer = "d"
IF in$ = "Epsilon" THEN beyer = "e"
IF in$ = "Phi" THEN beyer = "f"
IF in$ = "Gamma" THEN beyer = "g"
IF in$ = "Eta" THEN beyer = "h"
IF in$ = "Iota" THEN beyer = "i"
'IF in$ = "Psi" THEN beyer = "j"
IF in$ = "Kappa" THEN beyer = "k"
IF in$ = "Lambda" THEN beyer = "l"
IF in$ = "Mu" THEN beyer = "m"
IF in$ = "Nu" THEN beyer = "n"
IF in$ = "Omicron" THEN beyer = "o"
IF in$ = "Pi" THEN beyer = "p"
IF in$ = "Theta" THEN beyer = "q"
IF in$ = "Rho" THEN beyer = "r"
IF in$ = "Sigma" THEN beyer = "s"
IF in$ = "Tau" THEN beyer = "t"
IF in$ = "Upsilon" THEN beyer = "u"
IF in$ = "Omega" THEN beyer = "w"
IF in$ = "Xi" THEN beyer = "x"
IF in$ = "Psi" THEN beyer = "y"
IF in$ = "Zeta" THEN beyer = "z"
END FUNCTION

'Robert Lane's basic Post Script routines - used with permission
'Robert now has a much better PS programme for a planisphere in
'production
'**************************************************************************
SUB dArc (centerX, centerY, radius, startAngle, endAngle)
PRINT #1, "newpath 0.2 setlinewidth ";
PRINT #1, centerX; " "; centerY; " "; radius; " "; startAngle; " "; endAngle;
PRINT #1, " arc stroke "
END SUB

SUB dCircle (centerX, centerY, radius)
'Arc (centerX as double, centerY as double, radius, startAngle, endAngle)
'start and end angles are measured counterclockwise from positive x axis,
'in degrees
PRINT #1, "newpath 0.20 setlinewidth ";
PRINT #1, centerX; " "; centerY; " "; radius; " "; 0; " "; 360;
PRINT #1, " arc stroke "
END SUB

SUB dCircleFill (centerX, centerY, radius)
'Draws a filled circle with centre X,Y and radius R
PRINT #1, "newpath 0.20 setlinewidth ";
PRINT #1, centerX; " "; centerY; " "; radius; " "; 0; " "; 360;
PRINT #1, " arc fill "
END SUB

SUB dClose
PRINT #1, " showpage"
CLOSE 1
END SUB

SUB dLine (origX, origY, endX, endY)
PRINT #1, "newpath 0.2 setlinewidth ";
PRINT #1, origX; origY; " moveto ";
PRINT #1, endX; endY; " lineto";
PRINT #1, " stroke "
END SUB

SUB dRect (botX, botY, topX, topY)

PRINT #1, "newpath 0.2 setlinewidth"
PRINT #1, botX, botY, "moveto ";
PRINT #1, botX, topY, "lineto ";
PRINT #1, topX, topY, "lineto ";
PRINT #1, topX, botY, "lineto ";
PRINT #1, botX, botY, "lineto ";
PRINT #1, "closepath stroke "

END SUB

SUB dText (x, y, text$, fontsize)
' This hack was put together by kburnett, by working backwards from some PS...
font$ = "Helvetica"
IF fontsize <= 0 THEN fontsize = 10
PRINT #1, "newpath ";
PRINT #1, x; y; "moveto /"; font$; " findfont"; fontsize; " scalefont setfont "; "( "; text$; ") show "
END SUB

SUB dTextRgreek (x, y, text$, fontsize, angle)
' Prints the string in the font size specified
' at the given angle (clockwise) from horizontal
' about the centre 0,0 NOT relative to text position
IF fontsize < 4 THEN fontsize = 4
PRINT #1, "/Symbol findfont ";
PRINT #1, fontsize; " scalefont setfont";
PRINT #1, " newpath";
PRINT #1, x; y; " moveto ";
PRINT #1, angle; " rotate ";
PRINT #1, "("; text$; ") show ";
PRINT #1, -angle; " rotate "
END SUB

SUB dTextRotate (x, y, text$, fontsize, font$, angle)
' Prints the string in the font size specified
' at the given angle (clockwise) from horizontal
' about the centre 0,0 NOT relative to text position
IF fontsize < 4 THEN fontsize = 4
PRINT #1, "/"; font$; " findfont";
PRINT #1, fontsize; " scalefont setfont";
PRINT #1, " newpath";
PRINT #1, x; y; " moveto ";
PRINT #1, angle; " rotate ";
PRINT #1, "("; text$; ") show ";
PRINT #1, -angle; " rotate "
END SUB

SUB INIT (thefile$)

OPEN thefile$ + ".ps" FOR OUTPUT AS #1
PRINT #1, "%!PS-Adobe-1.0"
PRINT #1, "%%BoundingBox: 0 0 500 500"
END SUB

SUB stereo (a, z, x, y)
'takes the horizon coordinates of a star at a given instant
'and returns the x,y coords in the plane corresponding to
'a stereographic projection based on the Zentith point
'x and y have max values of 1
zalt = 1.570796
x = COS(z) * TAN((zalt - a) / 2)
y = SIN(z) * TAN((zalt - a) / 2)
END SUB

