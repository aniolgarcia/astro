size(16cm, 0);
import solids;
import graph3;
import three;

currentprojection=orthographic(15,10,5);

real r=10, h=6; // r=sphere radius; h=altitude section
triple Op=(0,0,h);

limits((0,0,0),1.1*(r,r,r));
//axes3("$S=180$","y","z");

pen p=rgb(1,0,0);
draw(Label("$N=0^{\circ}$",1),O--r*X,p,Arrow3);
draw(Label("$S=180^{\circ}$",1),O--(-r*X),p,Arrow3);
draw(Label("$W=270^{\circ}$",1),O--r*Y,p+dashed,Arrow3);
draw(Label("$E=90^{\circ}$",1),O--(-r*Y),p+dashed,Arrow3);

path3 arcU=Arc(O,r,90,0,0,0,Y,10);
revolution sphereU=revolution(O,arcU,Z);
draw(surface(sphereU), opacity(0.25)+lightgrey);

real width = 5;
real alt = 40;
real az = 58;

triple star = r*dir(90 - alt, az);
dot("s", star, SE, linewidth(width));
dot("zenith", r*dir(0, 0), N, linewidth(width));

draw("$\rho$",(0,0,0)-- r*dir(90, az), dashed);
draw(Label("altitude", 1.7), arc(O,r*Z,r*dir(90,az)),dashed);
draw(O--star,Arrow3,PenMargin3);
draw("$\lambda$",arc(O,0.15*star,0.15*r*dir(90,az)),E);
draw(Label("azimuth", 10), arc(O, 0.15*2*r, 90,0,90,az,CW), dashed, arrow = Arrow3(TeXHead2)); 
