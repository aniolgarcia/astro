size(16cm, 0);
import solids;
import graph3;
import three;

currentprojection=orthographic(3, 1, 1);
currentlight=nolight;

real RE=0.6, RS=0.7, inc=100, lat=45, lon=45, tlat=50, tlon=100;


revolution Earth=sphere((0,0,0), RE, n=2); 
draw(surface(Earth, n=4), surfacepen=lightgrey+opacity(.6), meshpen=0.6*white);
